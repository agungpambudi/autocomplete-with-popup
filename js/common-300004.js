var NUMBER_OF_CS_IMG = 2;

// Common, global functions should be placed inside this object to avoid name
// clashes. Think of this as a poor man's namespace.
window.Tv = {};

$.ajaxSetup({
  type        : 'POST',
  contentType : "application/json; charset=utf-8",
  dataType    : "json"
});

var dayNames = ['Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu'];
var earliestPagiTime  = 4*60  + 0, // convert to min in a day
    earliestSiangTime = 11*60 + 0,
    earliestSoreTime  = 15*60 + 0,
    earliestMalamTime = 18*60 + 30;

var maxNumUrlHistory = 10;
var maxUrlLengthInCookie = 70;

var setMaxCharInField = function (element, maxChar) {
  $(element).keypress(function(e) {
    var tval = $(element).val(),
        tlength = tval.length,
        set = maxChar,
        remain = parseInt(set - tlength);
    if (remain <= 0 && e.which !== 0 && e.charCode !== 0) {
      $(element).val((tval).substring(0, tlength - 1))
    }
  })
};

/**
 * Use goog.string.padNumber() instead.
 * @deprecated
 */
function formatTime(time) {
  if ((time*1) < 10){
    return '0'+time;
  }
  return time;
}

//usage "{0} is big, but {1} is small! {0} {2}".format("elephant", "ant")
String.prototype.format = function() {
  var args = arguments;
  return this.replace(/{(\d+)}/g, function(match, number) {
    return typeof args[number] != 'undefined'
      ? args[number]
      : match
    ;
  });
};

// Capitalize.
String.prototype.capitalize = function() {
    return this.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
};

// to use indexOf in IE
if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length >>> 0;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}

// to use forEach in IE
if (!Array.prototype.forEach)
{
  Array.prototype.forEach = function(fun /*, thisArg */)
  {
    "use strict";

    if (this === void 0 || this === null)
      throw new TypeError();

    var t = Object(this);
    var len = t.length >>> 0;
    if (typeof fun !== "function")
      throw new TypeError();

    var thisArg = arguments.length >= 2 ? arguments[1] : void 0;
    for (var i = 0; i < len; i++)
    {
      if (i in t)
        fun.call(thisArg, t[i], i, t);
    }
  };
}

// to use trim in IE
if (typeof String.prototype.trim !== 'function') {
  String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, '');
  }
}

// Return an Object that maps key to value
function getCookies() {
  var cookies = {};
  if (document.cookie == null || document.cookie == "")
    return cookies;

  var keyValueArray = document.cookie.split(";");
  for (var i = 0; i < keyValueArray.length; i++) {
    var s = keyValueArray[i];
    var indexOfEqual = s.indexOf("="); // Take the first '=' assuming key doesn't contain '='
    var key = s.substr(0, indexOfEqual).trim();
    var value = s.substr(indexOfEqual + 1);
    cookies[key] = value;
  }
  return cookies;
}

// cookies: an Object that maps key to value
// Note: document.cookie convention: key=value; [expires=Fri, 3 Aug 2001 20:47:11 UTC;] path=/
function setCookies(key, value, expireSec) {

}

// Basically encode ';', ","
function encodeUrlForCookie(raw) {
}

function decodeUrlForCookie(encoded) {
}

// urls is an array
function encodeUrlsForCookie(urls) {
}

function decodeUrlsForCookie(encoded) {
}

function putUrlToCookie(url) {
}

$(function () {
  
  // make dialog position fixed when scrolled
  if($.ui){
    $.ui.dialog.prototype._oldinit = $.ui.dialog.prototype._init;
    $.ui.dialog.prototype._init = function() {
        $(this.element).parent().css('position', 'fixed');
        $(this.element).dialog("option",{
            resizeStop: function(event,ui) {
                var position = [(Math.floor(ui.position.left) - $(window).scrollLeft()),
                                (Math.floor(ui.position.top) - $(window).scrollTop())];
                $(event.target).parent().css('position', 'fixed');
                $(event.target).parent().dialog('option','position',position);
                return true;
            }
        });
        this._oldinit();
    };
  }


  //=======================================================

  $('body').click(function(e) {
      if ( !jQuery(e.target).is('.ui-dialog, a') && !jQuery(e.target).closest('.ui-dialog').length )
        closeAllDialogBox();
  });

});

//value is integer value
function toDelimitedCurrency(value, partitionSize, delimiter) {
}

function toDelimitedCurrencyBold(value) {
 
}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

// all the functions below are related to login / register popup

var dialogList = [];

function closeAllDialogBox() {
  while ( dialogList.length != 0 ) {
    var dialogBox = dialogList.pop();
    dialogBox.dialog('destroy').remove();
  }
}


function openPopupWindow(url, title, w, h) {
  var l = (screen.width/2)-(w/2),
      t = (screen.height/2)-(h/2);
  return window.open(url, title, 'width='+w+ ',height='+h+ ',top='+t+ ',left='+l+ ',scrollbars=yes');
}

function waitingProcess(id, msg) {
}

function releaseWaitingProcess(id, msg) {
}

function displayNotification(msg) {
}

function displayError(id1, id2, err) {
}

function clearError(id1, id2) {
}

function internalServerError() {
}

function preventPageCloseHook(message) {
}

// Add native indexOf function that is not supported by IE < 9
// https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Array/indexOf
if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function (searchElement , fromIndex) {
    var i,
        pivot = (fromIndex) ? fromIndex : 0,
        length;

    if (!this) {
      throw new TypeError();
    }

    length = this.length;

    if (length === 0 || pivot >= length) {
      return -1;
    }

    if (pivot < 0) {
      pivot = length - Math.abs(pivot);
    }

    for (i = pivot; i < length; i++) {
      if (this[i] === searchElement) {
        return i;
      }
    }
    return -1;
  };
}



/**
 * Returns true if the airportCode is part of an airportArea.
 *
 * @param string airportCode
 * @param array airportAreas
 * @return bool
 */
function isAirportBelongsToAirportArea(airportCode, airportAreas) {
  var i, j, airportArea;
  for (i in airportAreas) {
    airportArea = airportAreas[i];
    for (j in airportArea.airportIds) {
      if (airportArea.airportIds[j] == airportCode) {
        return true;
      }
    }
  }
  return false;
}

/**
 * @param string airportCode
 * @param array airportAreas
 * @return bool
 */
function isAirportArea(airportCode, airportAreas) {
  var i, j, airportArea;
  for (i in airportAreas) {
    airportArea = airportAreas[i];
    if (airportArea.airportAreaId == airportCode) {
      return true;
    }
  }
  return false;
}

function getAirportOrArea(airportCode, attribute) {
  var a = airports[airportCode] != null ? airports[airportCode] : airportAreas[airportCode];

  return a;
}